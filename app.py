#-*- encoding: utf-8 -*-
from flask import Flask, request, flash, url_for, redirect, render_template, jsonify ,make_response 
from flask_restful import reqparse, abort, Api, Resource
import json
from werkzeug.utils import secure_filename
import sqlite3 as sql
import os
import sys
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

app = Flask(__name__)   
app.config['SECRET_KEY'] = config['DEFAULT']['SECRET_KEY']
app.config['JSON_AS_ASCII'] = config['SETTING']['JSON_AS_ASCII']
app.config["IMAGE_UPLOADS"] = config['SETTING']['IMAGE_UPLOADS']
DATABASE = config['DEFAULT']['DATABASE'] 

# create a database connection to the SQLite database
def create_connection(db_file):
    conn = None
    try:
        conn = sql.connect(db_file)
    except Error as e:
        print(e)
    return conn

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

# 관리자 메인 
@app.route('/')  
def main():  
   return render_template('main.html')  

# web list
@app.route('/list_employees', methods = ['GET', 'POST'])  
def list_employees():     
   img_src = request.host_url+"static/"

   query = "SELECT"
   query +=" a.emp_name, a.joindate, a.company, a.business, a.workarea, '"+img_src+"'||emp_img as emp_img ,ifnull(b.rating,0) rating"
   query +=" FROM employees a left outer join (SELECT emp_name,sum(rating) rating "
   query +=" FROM employee_rating GROUP BY emp_name) b ON a.emp_name=b.emp_name"
   
   # create a database connection
   conn = create_connection(DATABASE)
   conn.row_factory = sql.Row   
   cur = conn.cursor()
   cur.execute(query)
   rows = cur.fetchall(); 

   conn.close()
   return render_template("list_employees.html",rows = rows)


# web add employee
@app.route('/addEmployee',methods = ['POST', 'GET'])
def addEmployee():
   if request.method == 'POST':
      if not request.form['emp_name'] or not request.form['joindate'] or not request.form['company'] or not request.form['business'] or not request.form['workarea']:
         flash('Please enter all the fields', 'error')
      else:
         emp_name = request.form['emp_name']
         joindate = request.form['joindate']
         company  = request.form['company']
         business = request.form['business']
         workarea = request.form['workarea']            
         emp_img  = request.files["emp_img"]

         query = "INSERT INTO employees (emp_name,joindate,company,business,workarea,emp_img) VALUES (?,?,?,?,?,?)"

         try:
            filename = secure_filename(emp_img.filename)
            emp_img.save(os.path.join(app.config["IMAGE_UPLOADS"], filename))

            conn = create_connection(DATABASE)
            with conn:                        
               cur = conn.cursor()               
               cur.execute(query,(emp_name,joindate,company,business,workarea,filename)) 

               rows = cur.fetchall()
               conn.commit()
               flash('Record was successfully added')  
               return redirect(url_for('list_employees'))  
         except:
            conn.rollback()
            flash('error in insert operation')  
            return redirect(url_for('list_employees'))           
         finally:
            conn.close()

   return render_template("add.html")


# employees list json
@app.route('/list_all', methods = ['GET', 'POST'])  
def list_all():  
   img_src = request.host_url+"static/"
   
   query = "SELECT"
   query +=" a.emp_name as title, a.joindate, a.company, a.business as description, a.workarea,"
   query +="  '"+img_src+"'||emp_img as imageUrl ,ifnull(b.rating,0) rating"
   query +=", 'https://namu.wiki/w/' as web "
   query +=" FROM employees a left outer join (SELECT emp_name,sum(rating) rating "
   query +=" FROM employee_rating GROUP BY emp_name) b ON a.emp_name=b.emp_name"

   conn = create_connection(DATABASE)
   with conn:
      conn.row_factory = dict_factory
      cur = conn.cursor()
      cur.execute(query)
      rows = cur.fetchall()

   conn.close()
   #return jsonify({"items":rows})
   obj = {
            "version": "2.0",
            "template":{
               "outputs":{
                  "listCard":{
                     "header":{
                       "title": "카카오 i 디벨로퍼스를 소개합니다",
                       "imageUrl": "http://k.kakaocdn.net/dn/xsBdT/btqqIzbK4Hc/F39JI8XNVDMP9jPvoVdxl1/2x1.jpg"
                     },
                     "items":rows,
                     "buttons": [
                     {
                       "label": "구경가기",
                       "action": "webLink",
                       "webLinkUrl": "https://namu.wiki/w/%EC%B9%B4%EC%B9%B4%EC%98%A4%ED%94%84%EB%A0%8C%EC%A6%88"
                     }
                   ]
                  }
               }
            }
         }

   result = json.dumps(obj  ,ensure_ascii=False, sort_keys=False)
   res = make_response(result)
   return res

# 지역 like 검색
# http POST :5000/get_workarea workarea=여
@app.route('/get_workarea', methods = ['GET', 'POST'])  
def get_workarea():  
   #workarea = request.json["workarea"]
   workarea = request.form["workarea"]

   conn = create_connection(DATABASE)
   with conn:
      conn.row_factory = dict_factory
      cur = conn.cursor()
      cur.execute("select * from employees where workarea like '%"+ workarea+ "%'")
      rows = cur.fetchall()

   conn.close()

   result = json.dumps(rows ,ensure_ascii=False, sort_keys=False)
   res = make_response(result)
   return res
   #return jsonify({"data":rows})

# 평점입력   
# http POST :8080/rating emp_name=홍길동 rating=3
# http POST 52.15.136.240:8080 rating emp_name=홍길동 rating=3
# curl --header "Content-Type: application/json" --request POST --data '{"emp_name":"홍길동","rating":"2"}' http://52.15.136.240:8080/rating
@app.route('/rating',methods = ['POST', 'GET'])
def rating():
    
   if request.method == 'POST':
      if not request.form['emp_name'] or not request.form['rating']:
      #if not request.json['emp_name'] or not request.json['rating']:
         flash('Please enter all the fields', 'error')
      else:
         emp_name = request.form['emp_name']
         rating   = request.form['rating']

         try:
            conn = create_connection(DATABASE)
            with conn:
               cur = conn.cursor()
               query = "INSERT INTO employee_rating (emp_name,rating) VALUES (?,?)"               
               cur.execute(query, (emp_name,rating)) 

               rows = cur.fetchall()
               conn.commit()
               msg = "Record successfully added"
               #return msg
               return redirect(url_for('list_employees'))  
         except:
            conn.rollback()
            msg = "error in insert operation"
            return msg
         finally:
            conn.close()
   return "Error Form Type"

@app.route("/test", methods=['POST'])
def test():
   print(request.json)
   obj = {
    version: "2.0",
    template: {
      outputs: [
        {
          simpleImage: {
            imageUrl: "https://t1.daumcdn.net/friends/prod/category/M001_friends_ryan2.jpg",
            altText: "hello I'm Ryan"
          }
        }
      ]
    }
  }

   result = json.dumps(obj  ,ensure_ascii=False, sort_keys=False)
   res = make_response(result)
   return res


# Json 전송 테스트 Params 추출
@app.route("/show-counsel-list", methods=['POST'])
def show_counsel_list():  
   print(request)
   #print(json.loads(request))
   #data = json.loads(request.form['data'])   
   #dic = data['userRequest']['params']
   
   #print(dic['sys_location'])
   #for key,value in dic.items():
   # print(key,'->',value)

   #print(dic)
   #print('~~~~~~~~~') 
   #print(data)
   obj = {
            "version": "2.0",
            "template": {
               "outputs": [
                  {
                  "carousel": {
                     "type": "basicCard",
                     "items": [
                        {
                        "title": "이다은",
                        "description": "친절도 5.0, 실적 4.5",
                        "thumbnail": {
                           "imageUrl": "http://kbtest.dothome.co.kr/images/kb_uber/r1.jpg"
                        },
                        "buttons": [
                           {
                              "action": "message",
                              "label": "직원연결",
                              "messageText": "위치 검색 중"
                           },
                           {
                              "action":  "webLink",
                              "label": "구경하기",
                              "webLinkUrl": "https://www.kbstar.com/"
                           }
                        ]
                        },
                        {
                        "title": "조진석",
                        "description": "친절도 4.3, 실적 4.8",
                        "thumbnail": {
                           "imageUrl": "http://kbtest.dothome.co.kr/images/kb_uber/r2.jpg"
                        },
                        "buttons": [
                           {
                              "action": "message",
                              "label": "직원연결",
                              "messageText": "위치 검색 중"
                           },
                           {
                              "action":  "webLink",
                              "label": "구경하기",
                              "webLinkUrl": "https://www.kbstar.com/"
                           }
                        ]
                        },
                        {
                        "title": "조태원",
                        "description": "친절도 4.8, 실적 4.1",
                        "thumbnail": {
                           "imageUrl": "http://kbtest.dothome.co.kr/images/kb_uber/r3.jpg"
                        },
                        "buttons": [
                           {
                              "action": "message",
                              "label": "직원연결",
                              "messageText": "위치 검색 중"
                           },
                           {
                              "action":  "webLink",
                              "label": "구경하기",
                              "webLinkUrl": "https://www.kbstar.com/"
                           }
                        ]
                        },
                        {
                        "title": "이봉림",
                        "description": "친절도 4.9, 실적 4.0",
                        "thumbnail": {
                           "imageUrl": "http://kbtest.dothome.co.kr/images/kb_uber/r4.jpg"
                        },
                        "buttons": [
                           {
                              "action": "message",
                              "label": "직원연결",
                              "messageText": "위치 검색 중"
                           },
                           {
                              "action":  "webLink",
                              "label": "구경하기",
                              "webLinkUrl": "https://www.kbstar.com/"
                           }
                        ]
                        },
                        {
                        "title": "김은기",
                        "description": "친절도 4.6, 실적 4.6",
                        "thumbnail": {
                           "imageUrl": "http://kbtest.dothome.co.kr/images/kb_uber/r5.jpg"
                        },
                        "buttons": [
                           {
                              "action": "message",
                              "label": "직원연결",
                              "messageText": "위치 검색 중"
                           },
                           {
                              "action":  "webLink",
                              "label": "구경하기",
                              "webLinkUrl": "https://www.kbstar.com/"
                           }
                        ]
                        }
                     ]
                  }
                  }
               ]
            }
         }

   result = json.dumps(obj  ,ensure_ascii=False, sort_keys=False)
   res = make_response(result)
   return res
   #return jsonify(dic) 

# json 전송 테스트 화면
@app.route('/json_test')  
def json_test():  
   return render_template('json_test.html')  
 
if __name__ == '__main__':  
   #db.create_all()  
   #app.run(debug = True)  
   app.run(host='0.0.0.0',port=8080,debug = True)

# 실행
# nohup python app.py &

# http post 테스트
# pip install httpie
# http POST :5000/get_employees emp_name="박명수"

# amazon python 3 변경
# sudo rm python
# sudo ln -s python3.6 python

