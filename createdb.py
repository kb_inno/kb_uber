import sqlite3

conn = sqlite3.connect('kbdatabase.db')
print("Opened database successfully")

conn.execute('CREATE TABLE employees (emp_name TEXT, joindate TEXT, company TEXT, business TEXT, workarea TEXT, emp_img TEXT)')
print("employees Table created successfully")
conn.execute('CREATE TABLE employee_rating (emp_name TEXT, rating INT)')
print("employee_rating Table created successfully")
conn.close()
